# Columinator API

## Development

### Requirements

- Install [`pyenv`](https://github.com/pyenv/pyenv) with [pyenv installer](https://github.com/pyenv/pyenv-installer)
- Install python 3.8.3

```
pyenv install 3.8.3
```

- Install [`poetry`](https://python-poetry.org/docs/)
- Create pyenv virtual env

```
pyenv virtualenv 3.8.3 columinator
```

- enter `columinator` directory (`columinator` virtual env should be enabled)

```
cd api
```

- install dependencies

```
poetry install
```

### Prepare server

- add `.env` file (use `.env.sample` template)

- apply migrations

```
python manage.py migrate
```

- create super user

```
python manage.py createsuperuser
```

### Run server

- run server

```
python manage.py runserver
```
