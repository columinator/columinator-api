from django.db import connection
from django.test.utils import CaptureQueriesContext


def log_sql(fn):
    def wrapper(*args, **kwargs):
        with CaptureQueriesContext(connection) as ctx:
            r = fn(*args, **kwargs)
            for q in ctx.captured_queries:
                print('===============', fn.__name__, q['time'])
                print(q['sql'])
            return r

    return wrapper
