import yaml

from django.core.exceptions import ValidationError
from django.forms import CharField
from django.forms.widgets import Textarea
from django.utils.translation import gettext_lazy as _


class InvalidYAMLInput(str):
    pass


class YAMLString(str):
    pass


class YAMLField(CharField):
    default_error_messages = {'invalid': _('Enter a valid YAML.')}
    widget = Textarea

    def __init__(self, encoder=None, decoder=None, **kwargs):
        self.encoder = encoder
        self.decoder = decoder
        super().__init__(**kwargs)

    def to_python(self, value):
        if self.disabled:
            return value
        if value in self.empty_values:
            return None
        elif isinstance(value, (list, dict, int, float, YAMLString)):
            return value
        try:
            converted = yaml.safe_load(value)
        except yaml.YAMLError:
            raise ValidationError(
                self.error_messages['invalid'], code='invalid', params={'value': value}
            )
        if isinstance(converted, str):
            return YAMLString(converted)
        else:
            return converted

    def bound_data(self, data, initial):
        if self.disabled:
            return initial
        try:
            return yaml.safe_load(data)
        except yaml.YAMLError:
            return InvalidYAMLInput(data)

    def prepare_value(self, value):
        if isinstance(value, InvalidYAMLInput):
            return value
        return yaml.safe_dump(value)

    def has_changed(self, initial, data):
        if super().has_changed(initial, data):
            return True
        # For purposes of seeing whether something has changed, True isn't the
        # same as 1 and the order of keys doesn't matter.
        return yaml.safe_dump(initial, sort_keys=True) != yaml.safe_dump(
            self.to_python(data), sort_keys=True
        )
