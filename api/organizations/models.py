from django.db import models

from users.models import User
from utils.base_enum import BaseEnum


class Organization(models.Model):
    name = models.CharField(max_length=255, unique=True)
    members = models.ManyToManyField(
        User, related_name='organizations', through='OrganizationMembership'
    )

    def __str__(self):
        return self.name


class OrganizationMembershipRole(BaseEnum):
    OWNER = 'OWNER'
    ADMIN = 'ADMIN'
    USER = 'USER'


class OrganizationMembership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    role = models.CharField(
        max_length=255, choices=OrganizationMembershipRole.choices()
    )

    def __str__(self):
        return "{} in {}".format(self.user.username, self.organization.name)
