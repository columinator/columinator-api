from typing import Union

from django.contrib.auth.models import AnonymousUser
from django.db.models import QuerySet

from .models import Organization
from users.interactors import is_authenticated_user
from users.models import User


def get_all_organizations(
    *, queryset: 'Union[QuerySet[Organization], None]' = None
) -> 'QuerySet[Organization]':
    return queryset or Organization.objects


def get_organization_by_name(
    name, *, queryset: 'Union[QuerySet[Organization], None]' = None
):
    try:
        return get_all_organizations(queryset=queryset).get(name=name)
    except:
        return None


def get_organization_by_id(
    organization_id, *, queryset: 'Union[QuerySet[Organization], None]' = None
):
    try:
        return get_all_organizations(queryset=queryset).get(id=organization_id)
    except:
        return None


def get_organizations_visible_by_viewer(
    *,
    viewer: 'Union[User, AnonymousUser]',
    queryset: 'Union[QuerySet[Organization], None]' = None,
) -> 'QuerySet[Organization]':
    if is_authenticated_user(viewer=viewer):
        return get_all_organizations(queryset=queryset).filter(members__in=[viewer])
    return Organization.objects.none()
