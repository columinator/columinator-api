from graphene import relay

from graphql_api.schema import Extension, ViewerNode
from .organization_node import OrganizationNode, OrganizationConnection


class OrganizationQuery(Extension):
    class Meta:
        base = ViewerNode

    organizations = relay.ConnectionField(OrganizationConnection, required=True)

    def resolve_organizations(self, info):
        return OrganizationNode.get_queryset(queryset=None, info=info)

    organization = relay.Node.Field(OrganizationNode)


OrganizationQuery.register()
