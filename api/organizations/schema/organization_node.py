import graphene
from graphene import relay
from graphene_django import DjangoObjectType

from organizations.interactors import get_organizations_visible_by_viewer
from organizations.models import Organization


class OrganizationNode(DjangoObjectType):
    class Meta:
        model = Organization
        fields = ['name']
        interfaces = (relay.Node,)

    @classmethod
    def get_queryset(cls, queryset, info):
        return get_organizations_visible_by_viewer(
            viewer=info.context.user, queryset=queryset
        )


class OrganizationConnection(graphene.Connection):
    class Meta:
        node = OrganizationNode
