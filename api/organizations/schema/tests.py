import json

from graphene_django.utils import GraphQLTestCase
from graphql_relay import to_global_id
from sspipe import px, p

from graphql_api.schema import schema
from organizations.models import (
    Organization,
    OrganizationMembership,
    OrganizationMembershipRole,
)
from organizations.schema import OrganizationNode
from users.models import User


class MeQueryTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        viewer = User.objects.create_user(username='viewer', password='the-password')
        self._client.login(username='viewer', password='the-password')
        o1 = Organization.objects.create(name='o1')
        Organization.objects.create(name='o2')
        OrganizationMembership.objects.create(
            user=viewer, organization=o1, role=OrganizationMembershipRole.OWNER
        )
        self.o1 = o1

    def test_organizations_query(self):
        response = self.query(
            '''
            query {
              viewer {
                organizations {
                  edges { node { id name } }
                } 
              }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        organizations = (
            content['data']['viewer']['organizations']['edges']
            | p.map(px['node'])
            | p(list)
        )
        o1 = organizations | p(filter, px['name'] == 'o1') | p(next, None)
        o2 = organizations | p(filter, px['name'] == 'o2') | p(next, None)

        self.assertIsNotNone(o1)
        self.assertIsNone(o2)

    def test_organization_query(self):
        def query_organization(name):
            uid = Organization.objects.get(name=name).id
            response = self.query(
                '''
                query ($id: ID!) {
                  viewer { organization(id: $id) { name } }
                }
                ''',
                variables={'id': to_global_id(OrganizationNode._meta.name, uid)},
            )
            content = json.loads(response.content)

            self.assertResponseNoErrors(response)
            return content['data']['viewer']['organization']

        o1 = query_organization('o1')
        o2 = query_organization('o2')

        self.assertIsNotNone(o1)
        self.assertIsNone(o2)
