from django.contrib import admin
from organizations.models import Organization, OrganizationMembership


class OrganizationMembershipInline(admin.TabularInline):
    model = OrganizationMembership
    extra = 0


class OrganizationAdmin(admin.ModelAdmin):
    inlines = (OrganizationMembershipInline,)


admin.site.register(Organization, OrganizationAdmin)
