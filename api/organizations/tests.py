from django.contrib.auth.models import AnonymousUser
from django.test import TestCase

from .interactors import (
    get_organizations_visible_by_viewer,
    get_organization_by_name,
    get_organization_by_id,
)
from .models import Organization, OrganizationMembership, OrganizationMembershipRole
from users.models import User


class OrganizationTestCase(TestCase):
    def setUp(self) -> None:
        viewer = User.objects.create_user(username='viewer')
        o1 = Organization.objects.create(name='o1')
        Organization.objects.create(name='o2')
        OrganizationMembership.objects.create(
            user=viewer, organization=o1, role=OrganizationMembershipRole.OWNER
        )
        self.o1 = o1

    def test_str(self):
        o1 = Organization.objects.get(name='o1')
        self.assertEqual(str(o1), 'o1')

    def test_get_organizations_visible_by_viewer(self):
        viewer = User.objects.get(username='viewer')

        o1 = Organization.objects.get(name='o1')
        o2 = Organization.objects.get(name='o2')
        orgs = get_organizations_visible_by_viewer(viewer=viewer)
        self.assertIn(o1, orgs)
        self.assertNotIn(o2, orgs)

    def test_get_organizations_visible_by_viewer_anonymously(self):
        au = AnonymousUser()
        orgs = get_organizations_visible_by_viewer(viewer=au)
        self.assertEqual(len(orgs), 0)

    def test_get_organization_by_name(self):
        o1 = get_organization_by_name('o1')
        invalid = get_organization_by_name('invalid-name')
        self.assertEqual(o1.name, 'o1')
        self.assertIsNone(invalid)

    def test_organization_by_id(self):
        o1 = get_organization_by_id(self.o1.id)
        o1str = get_organization_by_id(str(self.o1.id))
        invalid = get_organization_by_id(-1)
        self.assertEqual(o1.name, 'o1')
        self.assertEqual(o1str.name, 'o1')
        self.assertIsNone(invalid)
