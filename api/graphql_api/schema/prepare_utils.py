from graphene.utils.get_unbound_function import get_unbound_function
from graphql import ResolveInfo
from graphql.execution.utils import should_include_node
from graphql.execution.values import get_argument_values
from graphql.language.ast import Field, FragmentSpread, InlineFragment, SelectionSet


def get_name(asts: 'Field'):
    return asts.name.value


def get_alias(asts: 'Field'):
    name = asts.name.value
    return asts.alias.value if asts.alias else name


def get_args(info: 'ResolveInfo', field_ast: 'Field', field_name: 'str'):
    field_def = info.parent_type.fields.get(field_name)
    return get_argument_values(
        field_def.args, field_ast.arguments, info.variable_values
    )


def get_type(a_type):
    while hasattr(a_type, 'of_type'):
        a_type = a_type.of_type
    return a_type


def get_prepare_for_field(info: 'ResolveInfo', name: 'str', prefix: 'str'):
    prepare_fn = getattr(
        info.parent_type.graphene_type, "{}_{}".format(prefix, name), None
    )
    if not prepare_fn:
        return None
    return get_unbound_function(prepare_fn)


def get_field_info(info: 'ResolveInfo', asts: 'Field'):
    field_name = get_name(asts)
    alias = get_alias(asts)
    parent_type = get_type(info.return_type)
    return_type = parent_type.fields.get(field_name).type

    return ResolveInfo(
        field_name=field_name,
        field_asts=[asts],
        return_type=return_type,
        parent_type=parent_type,
        schema=info.schema,
        fragments=info.fragments,
        root_value=info.root_value,
        operation=info.operation,
        variable_values=info.variable_values,
        context=info.context,
        path=info.path + [alias],
    )


def prepare_field(root, info: 'ResolveInfo', asts: 'Field', prefix: 'str'):
    if not should_include_node(info, asts.directives):
        return

    name = get_name(asts)
    prepare_fn = get_prepare_for_field(info, name, prefix)
    if not prepare_fn:
        return
    args = get_args(info, asts, name)
    prepare_fn(root, info, **args)


def prepare_inline_fragment(
    root, info: 'ResolveInfo', asts: 'InlineFragment', prefix: 'str'
):
    prepare_selection_set(root, info, asts.selection_set, prefix)


def prepare_fragment_spread(
    root, info: 'ResolveInfo', asts: 'FragmentSpread', prefix: 'str'
):
    name = asts.name.value
    fragment = info.fragments.get(name)
    return prepare_selection_set(root, info, fragment.selection_set, prefix)


def prepare_selection_set(
    root, info: 'ResolveInfo', asts: 'SelectionSet', prefix: 'str'
):
    for selection in asts.selections:
        if isinstance(selection, Field):
            prepare_field(root, get_field_info(info, selection), selection, prefix)
        if isinstance(selection, InlineFragment):
            prepare_inline_fragment(root, info, selection, prefix)
        if isinstance(selection, FragmentSpread):
            prepare_fragment_spread(root, info, selection, prefix)


def prepare(root, info: 'ResolveInfo', prefix='prepare'):
    asts = info.field_asts
    for field in asts:
        if hasattr(field, 'selection_set') and field.selection_set is not None:
            prepare_selection_set(root, info, field.selection_set, prefix)
