import graphene

from .utils import get_node


class ViewerNode(graphene.ObjectType):
    class Meta:
        interfaces = (graphene.relay.Node,)

    node = graphene.Field(graphene.relay.Node, id=graphene.NonNull(graphene.ID))

    def resolve_node(self, info, id):
        return get_node(global_id=id, info=info)

    @classmethod
    def get_node(cls, info, id):
        if id == "0":
            return ViewerNode(id=id)
        return None
