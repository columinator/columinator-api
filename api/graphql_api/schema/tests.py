import json

from graphene_django.utils.testing import GraphQLTestCase
from graphql_relay import to_global_id

from . import ViewerNode
from .schema import schema


class ViewerTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def test_viewer_query(self):
        viewer_node_id = to_global_id(ViewerNode._meta.name, '0')
        response = self.query(
            '''
            query {
              viewer { id }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(content['data']['viewer']['id'], viewer_node_id)

    def test_viewer_node_query(self):
        viewer_node_id = to_global_id(ViewerNode._meta.name, '0')
        response = self.query(
            '''
            query($id: ID!) {
              viewer { node(id: $id) { id __typename } }
            }
            ''',
            variables={"id": viewer_node_id},
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(content['data']['viewer']['node']['id'], viewer_node_id)
        self.assertEqual(
            content['data']['viewer']['node']['__typename'], ViewerNode._meta.name
        )


class NodeTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def test_node_query(self):
        viewer_node_id = to_global_id(ViewerNode._meta.name, '0')
        response = self.query(
            '''
            query($id: ID!) {
              node(id: $id) { id __typename }
            }
            ''',
            variables={"id": viewer_node_id},
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(content['data']['node']['id'], viewer_node_id)
        self.assertEqual(content['data']['node']['__typename'], ViewerNode._meta.name)
