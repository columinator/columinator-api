import graphene

from . import get_node
from .with_viewer import WithViewer


class RootQuery(graphene.ObjectType, WithViewer):
    node = graphene.Field(graphene.relay.Node, id=graphene.NonNull(graphene.ID))

    def resolve_node(self, info, id):
        return get_node(global_id=id, info=info)
