from collections import OrderedDict

from graphene import Field
from graphene.types.base import BaseType, BaseOptions
from graphene.types.utils import yank_fields_from_attrs


class ExtensionOptions(BaseOptions):
    fields = None
    base = None
    cls = None


class Extension(BaseType):
    @classmethod
    def __init_subclass_with_meta__(cls, base=None, _meta=None, **options):
        if not _meta:
            _meta = ExtensionOptions(cls)
        if not _meta.base:
            _meta.base = base
        _meta.cls = cls

        fields = OrderedDict()

        for base in reversed(cls.__mro__):
            fields.update(yank_fields_from_attrs(base.__dict__, _as=Field))

        _meta.fields = fields

        super(Extension, cls).__init_subclass_with_meta__(_meta=_meta)

    @classmethod
    def register(cls):
        fields = cls._meta.fields
        base = cls._meta.base

        for name in fields.keys():
            assert (
                name not in base._meta.fields
            ), "{name} already exists in {base}".format(name=name, base=base.__name__)

            assert not hasattr(
                base, "resolve_{}".format(name)
            ), "{base} already has {name} resolver".format(
                name=name, base=base.__name__
            )
            resolver = getattr(cls, "resolve_{}".format(name), None)
            if resolver:
                setattr(base, "resolve_{}".format(name), resolver)
        base._meta.fields.update(fields)
