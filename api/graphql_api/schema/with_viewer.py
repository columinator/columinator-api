from graphene import Field

from .viewer_node import ViewerNode


class WithViewer(object):
    viewer = Field(ViewerNode, required=True)

    def resolve_viewer(self, info):
        return ViewerNode(id="0")
