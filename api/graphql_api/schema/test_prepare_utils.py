from dataclasses import dataclass
from enum import Enum
from typing import Union, Dict
from unittest.mock import patch

import graphene
from graphene_django.utils import GraphQLTestCase
from graphql import ResolveInfo

from graphql_api.schema.prepare_utils import prepare

data = {
    "A": [5, 5, 3],
    "B": [8, 9, 2],
    "C": [10, 8, 3],
}


def get_alias(info: 'ResolveInfo'):
    return info.path[-1]


def calc_sum(field: str):
    return sum(data.get(field, [0]))


def calc_max(field: str):
    return max(data.get(field, [0]))


class Operator(Enum):
    SUM = 'sum'
    MAX = 'max'


@dataclass
class Operation:
    operator: 'Operator'
    field: 'str'


@dataclass
class ItemData:
    name: 'str'
    values: 'Dict[str, int]'


def calc_value(op: 'Operation'):
    if op.operator == Operator.MAX:
        return calc_max(op.field)
    if op.operator == Operator.SUM:
        return calc_sum(op.field)
    raise ValueError('Invalid operator')


def calc_values(ops: 'Dict[str, Operation]'):
    # this is expensive calculation, or query entrenchment e.g. django annotate
    return {alias: calc_value(op) for alias, op in ops.items()}


class Item(graphene.ObjectType):
    name = graphene.Field(graphene.String)
    sum = graphene.Field(graphene.Int, args={'field': graphene.String()})

    def prepare_sum(root: 'Dict[str, Operation]', info, **args):
        field = args.get('field')
        alias = get_alias(info)
        root[alias] = Operation(operator=Operator.SUM, field=field)

    def resolve_sum(root: ItemData, info, **args):
        alias = get_alias(info)
        return root.values[alias]

    max = graphene.Field(graphene.Int, args={'field': graphene.String()})

    def prepare_max(root: 'Dict[str, Operation]', info, **args):
        field = args.get('field')
        alias = get_alias(info)
        root[alias] = Operation(operator=Operator.MAX, field=field)

    def resolve_max(root: ItemData, info, **args):
        alias = get_alias(info)
        return root.values[alias]


class RootQuery(graphene.ObjectType):
    name = graphene.String()
    item = graphene.Field(Item)
    non_null_item = graphene.NonNull(Item)

    def resolve_name(self, info, **kwargs):
        prepare({}, info)  # do nothings
        return 'root'

    def resolve_item(self, info, **kwargs):
        root = {}
        prepare(root, info)
        values = calc_values(root)
        return ItemData(values=values, name='the item')

    def resolve_non_null_item(self, info, **kwargs):
        root = {}
        prepare(root, info)
        values = calc_values(root)
        return ItemData(values=values, name='the item')


schema = graphene.Schema(query=RootQuery)

#
# schema {
#   query: RootQuery
# }
#
# type Item {
#   name: String
#   sum(field: String): Int
#   max(field: String): Int
# }
#
# type RootQuery {
#   name: String
#   item: Item
#   nonNullItem:: Item!
# }
#


class ViewerTestCase(GraphQLTestCase):
    def test_prepare(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item {
                sum(field: "A")
              }
            }
            '''
        )
        self.assertEqual(response.data['item']['sum'], 13)

    def test_prepare_support_skip(self):
        with patch(__name__ + '.calc_sum') as fn:
            response = schema.execute(
                '''
                query ItemQuery {
                  item {
                  name
                    sum(field: "X") @skip(if: true)
                  }
                }
                '''
            )
            fn.assert_not_called()

        self.assertIsNone(response.data['item'].get('sum'))

    def test_prepare_support_alias(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item {
                maxB: max(field: "B")
              }
            }
            '''
        )
        self.assertEqual(response.data['item']['maxB'], 9)

    def test_prepare_support_fragment(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item {
              ...item_c
              }
            }

            fragment item_c on Item {
              sum(field: "C")
            }
            '''
        )
        self.assertEqual(response.data['item']['sum'], 21)

    def test_prepare_support_inline_fragment(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item {
                ... on Item {
                  sum(field: "A")
                }
              }
            }
            '''
        )
        self.assertEqual(response.data['item']['sum'], 13)

    def test_prepare_support_non_null_parent(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item: nonNullItem {
                sum(field: "A")
              }
            }
            '''
        )
        self.assertEqual(response.data['item']['sum'], 13)

    def test_prepare_leaves_other_fields(self):
        response = schema.execute(
            '''
            query ItemQuery {
              item {
                name
              }
            }
            '''
        )
        self.assertEqual(response.data['item']['name'], 'the item')

    def test_prepare_support_simple_fields(self):
        response = schema.execute(
            '''
            query ItemQuery {
              name
            }
            '''
        )

        self.assertEqual(response.data['name'], 'root')
