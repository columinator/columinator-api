from django.utils.module_loading import autodiscover_modules

__all__ = ['autodiscover']


def autodiscover():
    autodiscover_modules('schema')
