from .models import User


def get_all_valid_users(*, queryset=None):
    return (queryset or User.objects).filter(is_staff=False, is_active=True)


def get_user_by_id(*, queryset=None, user_id):
    try:
        return get_all_valid_users(queryset=queryset).get(id=user_id)
    except User.DoesNotExist:
        return None


def is_valid_user(*, user):
    return user.is_active and not user.is_staff and user.is_authenticated


def get_users(*, viewer, queryset=None):
    """
    :param viewer: current logged in user
    :param queryset: base User queryset
    :return: queryset of all visible users by viewer
    """
    if is_authenticated_user(viewer=viewer):
        return get_all_valid_users(queryset=queryset)
    return User.objects.none()


def is_authenticated_user(*, viewer):
    return is_valid_user(user=viewer)
