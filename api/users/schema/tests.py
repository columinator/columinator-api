import json

from graphene_django.utils.testing import GraphQLTestCase
from graphql_relay import to_global_id
from sspipe import p, px

from graphql_api.schema import schema
from users.models import User
from users.schema import UserNode


class MeQueryTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        User.objects.create_user(username='viewer', password='the-password')
        self._client.login(username='viewer', password='the-password')

    def test_me_query(self):
        response = self.query(
            '''
            query {
              viewer { me { username } }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(content['data']['viewer']['me']['username'], 'viewer')


class AnonymousMeQueryTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        User.objects.create_user(username='viewer', password='the-password')

    def test_me_query(self):
        response = self.query(
            '''
            query {
              viewer { me { username } }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(content['data']['viewer']['me'], None)


class UsersQueryTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        User.objects.create_user(username='u1', is_active=False)
        User.objects.create_user(username='u2', is_staff=True)
        User.objects.create_user(username='u3')
        User.objects.create_user(username='viewer', password='the-password')

        self._client.login(username='viewer', password='the-password')

    def test_users_query(self):
        response = self.query(
            '''
            query {
              viewer { users { edges { node { username } } } }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        users = (
            content['data']['viewer']['users']['edges'] | p.map(px['node']) | p(list)
        )

        u1 = users | p(filter, px['username'] == 'u1') | p(next, None)
        u2 = users | p(filter, px['username'] == 'u2') | p(next, None)
        u3 = users | p(filter, px['username'] == 'u3') | p(next, None)
        viewer = users | p(filter, px['username'] == 'viewer') | p(next, None)

        self.assertIsNone(u1)
        self.assertIsNone(u2)
        self.assertIsNotNone(u3)
        self.assertIsNotNone(viewer)

    def test_user_query(self):
        def query_user(username):
            uid = User.objects.get(username=username).id
            response = self.query(
                '''
                query ($id: ID!) {
                  viewer { user(id: $id) { username } }
                }
                ''',
                variables={'id': to_global_id(UserNode._meta.name, uid)},
            )
            content = json.loads(response.content)

            self.assertResponseNoErrors(response)
            return content['data']['viewer']['user']

        u1 = query_user('u1')
        u2 = query_user('u2')
        u3 = query_user('u3')
        viewer = query_user('viewer')

        self.assertIsNone(u1)
        self.assertIsNone(u2)
        self.assertIsNotNone(u3)
        self.assertIsNotNone(viewer)
