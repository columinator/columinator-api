import graphene
from graphene import relay
from graphene_django import DjangoObjectType

from users.interactors import get_users
from users.models import User


class UserNode(DjangoObjectType):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']
        interfaces = (relay.Node,)

    @classmethod
    def get_queryset(cls, queryset, info):
        return get_users(viewer=info.context.user, queryset=queryset)


class UserConnection(graphene.Connection):
    class Meta:
        node = UserNode
