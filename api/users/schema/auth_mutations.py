from graphql_api.schema import Extension, RootMutation
from .mutations.login_mutation import LoginMutation
from .mutations.logout_mutation import LogoutMutation


class AuthMutations(Extension):
    class Meta:
        base = RootMutation

    login = LoginMutation.Field(required=True)
    logout = LogoutMutation.Field(required=True)


AuthMutations.register()
