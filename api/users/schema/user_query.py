import graphene
from graphene import relay

from graphql_api.schema import Extension, ViewerNode
from users.interactors import get_users, is_authenticated_user
from users.schema.user_node import UserNode, UserConnection


class UserQuery(Extension):
    class Meta:
        base = ViewerNode

    users = relay.ConnectionField(UserConnection, required=True)

    def resolve_users(self, info):
        return get_users(viewer=info.context.user)

    user = relay.Node.Field(UserNode)
    me = graphene.Field(UserNode)

    def resolve_me(self, info):
        if is_authenticated_user(viewer=info.context.user):
            return info.context.user
        return None


UserQuery.register()
