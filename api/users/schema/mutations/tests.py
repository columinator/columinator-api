import json

from graphene_django.utils.testing import GraphQLTestCase

from graphql_api.schema import schema
from ...models import User


class LoginMutationTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        User.objects.create_user(username='test-user', password='the-password')
        User.objects.create_user(
            username='inactive-user', password='the-password', is_active=False
        )
        User.objects.create_user(
            username='staff-user', password='the-password', is_staff=True
        )

    def login_query(self, username, password):
        return self.query(
            '''
            mutation ($username: String!, $password: String!) {
              login(input: {username: $username, password: $password}) {
                user { username }
                viewer { me { username } } }
            }
            ''',
            variables={'username': username, 'password': password},
        )

    def test_login(self):
        response = self.login_query('test-user', 'the-password')
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertEqual(
            content['data']['login']['viewer']['me']['username'], 'test-user'
        )
        self.assertEqual(content['data']['login']['user']['username'], 'test-user')

    def test_invalid_login(self):
        response = self.login_query('test-user', 'bad-password')
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertIsNone(content['data']['login']['viewer']['me'])
        self.assertIsNone(content['data']['login']['user'])

    def test_inactive_user_login(self):
        response = self.login_query('inactive-user', 'the-password')
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertIsNone(content['data']['login']['viewer']['me'])
        self.assertIsNone(content['data']['login']['user'])

    def test_staff_user_login(self):
        response = self.login_query('staff-user', 'the-password')
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertIsNone(content['data']['login']['viewer']['me'])
        self.assertIsNone(content['data']['login']['user'])


class LogoutMutationTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self) -> None:
        User.objects.create_user(username='test-user', password='the-password')
        self._client.login(username='test-user', password='the-password')

    def test_logout_mutation(self):
        response = self.query(
            '''
            mutation {
              logout(input: {}) {
                viewer { me { username } } }
            }
            '''
        )
        content = json.loads(response.content)

        self.assertResponseNoErrors(response)
        self.assertIsNone(content['data']['logout']['viewer']['me'])
