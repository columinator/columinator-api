import graphene
from django.contrib.auth import authenticate, login, logout
from graphene import ClientIDMutation

from graphql_api.schema import WithViewer
from ...interactors import is_authenticated_user
from ..user_node import UserNode


class LoginMutation(WithViewer, ClientIDMutation):
    class Input:
        username = graphene.NonNull(graphene.String)
        password = graphene.NonNull(graphene.String)

    user = graphene.Field(UserNode)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        username = args['username']
        password = args['password']

        logout(info.context)

        user = authenticate(username=username, password=password)

        if user is None:
            return LoginMutation(user=None)
        if not is_authenticated_user(viewer=user):
            return LoginMutation(user=None)

        login(info.context, user)

        return LoginMutation(user=user)
