from django.contrib.auth.models import AnonymousUser
from django.test import TestCase

from users.interactors import (
    is_valid_user,
    get_all_valid_users,
    get_users,
    is_authenticated_user,
    get_user_by_id,
)
from users.models import User


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(username='u1', is_active=False)
        User.objects.create_user(username='u2', is_staff=True)
        User.objects.create_user(username='u3')
        User.objects.create_user(username='viewer')

    def test_is_valid_user(self):
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')

        self.assertFalse(is_valid_user(user=u1))
        self.assertFalse(is_valid_user(user=u2))
        self.assertTrue(is_valid_user(user=u3))

    def test_get_all_valid_users(self):
        viewer = User.objects.get(username='viewer')
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')

        users = get_all_valid_users()

        self.assertNotIn(u1, users)
        self.assertNotIn(u2, users)
        self.assertIn(u3, users)
        self.assertIn(viewer, users)

    def test_get_users(self):
        viewer = User.objects.get(username='viewer')
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')

        users = get_users(viewer=viewer)

        self.assertNotIn(u1, users)
        self.assertNotIn(u2, users)
        self.assertIn(u3, users)
        self.assertIn(viewer, users)

    def test_get_user_by_id(self):
        viewer = User.objects.get(username='viewer')
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')

        self.assertIsNone(get_user_by_id(user_id=u1.id))
        self.assertIsNone(get_user_by_id(user_id=u2.id))
        self.assertIsNotNone(get_user_by_id(user_id=u3.id))
        self.assertIsNotNone(get_user_by_id(user_id=viewer.id))

    def test_get_users_anonymously(self):
        au = AnonymousUser()

        users = get_users(viewer=au)

        self.assertEqual(len(users), 0)

    def test_is_authenticated_user(self):
        au = AnonymousUser()
        viewer = User.objects.get(username='viewer')
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')

        self.assertFalse(is_authenticated_user(viewer=au))
        self.assertFalse(is_authenticated_user(viewer=u1))
        self.assertFalse(is_authenticated_user(viewer=u2))

        self.assertTrue(is_authenticated_user(viewer=u3))
        self.assertTrue(is_authenticated_user(viewer=viewer))
