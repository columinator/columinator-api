coverage run --branch --source='.' -m pytest --junitxml=coverage/report.xml $@
coverage report
coverage html -d coverage/lcov-report
coverage xml -o coverage/clover.xml
coverage json -o coverage/coverage-final.json
