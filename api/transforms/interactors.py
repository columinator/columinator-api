import copy
from typing import List, Union

from transforms import BaseTransform


def create_transform(config: 'dict') -> 'Union[BaseTransform,None]':
    name = config.get('transform')
    transform_class = BaseTransform.get_transform(name)
    if transform_class is None:
        return None
    params = config.get('params', {})
    return transform_class(params=params)


def create_transforms(configs: 'List[dict]') -> 'List[BaseTransform]':
    maybe_transforms = [create_transform(config) for config in configs]
    return [
        maybe_transform
        for maybe_transform in maybe_transforms
        if maybe_transform is not None
    ]


def apply_transforms(data, *, transforms: 'List[BaseTransform]'):
    data = copy.deepcopy(data)
    for transform in transforms:
        transform.transform(data)
    return data
