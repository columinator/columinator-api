from django.test import TestCase

from transforms.interactors import create_transform, create_transforms, apply_transforms


class TransformTestCase(TestCase):
    def test_j2g_date_transform(self):
        transform = create_transform(
            {
                'transform': 'j2g-date',
                'params': {'from': 'date', 'to': 'date', 'format': '%Y/%m/%d'},
            }
        )
        value = {'date': '1399/09/09'}
        transform.transform(value)
        self.assertEqual(value, {'date': '2020-11-29'})

    def test_transforms(self):
        transforms = create_transforms(
            [
                {
                    'transform': 'j2g-date',
                    'params': {'from': 'date', 'to': 'date', 'format': '%Y/%m/%d'},
                }
            ]
        )
        value = {'date': '1399/09/09'}
        new_values = apply_transforms(value, transforms=transforms)
        self.assertEqual(value, {'date': '1399/09/09'})
        self.assertEqual(new_values, {'date': '2020-11-29'})

    def test_unknown_transforms(self):
        transforms = create_transforms([{'transform': 'unknown', 'params': {}}])
        value = {'date': '1399/09/09'}
        new_values = apply_transforms(value, transforms=transforms)
        self.assertEqual(value, {'date': '1399/09/09'})
        self.assertEqual(new_values, {'date': '1399/09/09'})
