import jdatetime

from .base_transform import BaseTransform


class JalaliToGregorianTransform(BaseTransform):
    class Meta:
        name = 'j2g-date'

    def transform(self, mutable_data: 'dict'):
        date_format = self.params.get('format')
        from_field = self.params.get('from')
        to_field = self.params.get('to')
        value = mutable_data.get(from_field)
        value = (
            jdatetime.datetime.strptime(value, format=date_format)
            .togregorian()
            .strftime('%Y-%m-%d')
        )
        mutable_data[to_field] = value
