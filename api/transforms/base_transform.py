from typing import Type, Union

from graphene.utils.subclass_with_meta import SubclassWithMeta


class BaseTransformOptions(object):
    name: str = None

    def __init__(self, class_type: Type):
        self.class_type: Type = class_type

    def __setattr__(self, name, value):
        super(BaseTransformOptions, self).__setattr__(name, value)

    def __repr__(self):
        return "<{} name={}>".format(self.__class__.__name__, repr(self.name))


class BaseTransform(SubclassWithMeta):
    transforms = {}

    def __init__(self, params):
        self.params = params

    @classmethod
    def __init_subclass_with_meta__(
        cls, name=None, description=None, _meta=None, **_kwargs
    ):
        assert "_meta" not in cls.__dict__, "Can't assign directly meta"
        if not _meta:
            _meta = BaseTransformOptions(cls)
        _meta.name = name or cls.__name__
        cls._meta = _meta
        BaseTransform.transforms[_meta.name] = cls
        super(BaseTransform, cls).__init_subclass_with_meta__()

    @staticmethod
    def get_transform(name: "str") -> "Union[Type[BaseTransform],None]":
        return BaseTransform.transforms.get(name, None)

    def transform(self, data: "dict"):
        pass

    def __repr__(self):
        return "<Transform name={} params={}>".format(
            self.__class__.__name__, repr(self._meta.name), repr(self.params)
        )
